package com.demo.kafkaQ.data;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="QueueFailures")
@Data
public class QueueFailures {
	@Id
	private String id;
	@Indexed
	@NotNull
	private String topicName;
	@NotNull
	private Object object;
	@Indexed
	private Date createdDate = new Date();
	
	public QueueFailures(String topicName, Object object) {
		super();
		this.topicName = topicName;
		this.object = object;
	}
}
