package com.demo.kafkaQ.constants;

public class SettingsConstant {

    public static final String FILENAME="kafkaconfig.properties";
    public static final String SERVER_ADDRESS="server_address";
    public static final String MAX_BLOCK_MS_CONFIG="max_block_ms_config";
    public static final String MAX_BLOCK_SIZE="max_block_size";
    public static final String MAX_TIME_WAITING = "max_time_waiting";
    public static final String QUEUE_FAILURE_COLLECTION_NAME = "QueueFailures";


}
