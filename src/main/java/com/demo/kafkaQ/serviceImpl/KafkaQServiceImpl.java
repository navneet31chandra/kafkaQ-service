package com.demo.kafkaQ.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.demo.kafkaQ.data.QueueFailures;
import com.demo.kafkaQ.service.KafkaQService;
import com.demo.kafkaQ.service.QueueFailureService;
import com.fasterxml.jackson.databind.ObjectMapper;


public class KafkaQServiceImpl implements KafkaQService {
	
	protected static final ObjectMapper objectmap=new ObjectMapper();
	protected final static Logger logger = LoggerFactory.getLogger(KafkaQServiceImpl.class);
	@Autowired
	KafkaTemplate kafkaTemplate;
	
	@Autowired
	QueueFailureService queueFailureService;

	@Override
	public boolean insert(final Object object, final String topic_name) {
		try {
			String object_as_string = objectmap.writeValueAsString(object);
			logger.info("topic_name {} Message string: {}",topic_name, object_as_string);
			ListenableFuture<SendResult<String,String> > future = kafkaTemplate.send(topic_name,object_as_string);			
			future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
				@Override
				public void onFailure(Throwable throwable) {
					try {
						logger.info("Topic failed to push to kafka :"+topic_name+" message :"+object);
						QueueFailures queueFailure = new QueueFailures(topic_name,object);
						queueFailureService.insert(queueFailure);
					} catch (Exception e) {
						logger.error("Error while inserting data into QueueFailures collection {} : {} : {}",topic_name ,object, e);
					}
					
				}

				@Override
				public void onSuccess(SendResult<String, String> sendResult) {
					logger.info("Success send to kafka with response {} and {} time", sendResult.getRecordMetadata());
				}
			});
		} catch (Exception e) {
			try {
				QueueFailures queueFailure = new QueueFailures(topic_name,object);
				queueFailureService.insert(queueFailure);
			} catch (Exception ex) {
				logger.error("Error while inserting data into QueueFailures collection {} : {} ",topic_name ,object);
			}
			return false;
		}
		return true;
	
	}

}
