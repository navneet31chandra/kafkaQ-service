package com.demo.kafkaQ.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;

import com.demo.kafkaQ.data.QueueFailures;
import com.demo.kafkaQ.repo.QueueFailuresRepo;

public class QueueFailuesRepoImpl implements QueueFailuresRepo{
	@Autowired
	QueueFailuresRepo queueFailureRepo;

	@Override
	public void insert(QueueFailures queueFailures) {
		queueFailureRepo.insert(queueFailures);
		return;
	}

}
