package com.demo.kafkaQ.service;

import com.demo.kafkaQ.data.QueueFailures;


public interface QueueFailureService {
	public void insert(QueueFailures queueFailure) throws Exception;
}
