package com.demo.kafkaQ.repoImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.demo.kafkaQ.constants.SettingsConstant;
import com.demo.kafkaQ.data.QueueFailures;
import com.demo.kafkaQ.repo.QueueFailuresRepo;

public class QueueFailuesRepoImpl implements QueueFailuresRepo {
	
	@Autowired
	MongoTemplate mongoTemplate;

	@Override
	public void insert(QueueFailures queueFailures) {
		mongoTemplate.insert(queueFailures, SettingsConstant.QUEUE_FAILURE_COLLECTION_NAME);
		
	}

}
